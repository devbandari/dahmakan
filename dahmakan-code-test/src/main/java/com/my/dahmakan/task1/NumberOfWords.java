package com.my.dahmakan.task1;


public class NumberOfWords {

	int getNumberOfWordsUsingStrems(String str) {
			// checks whether string is empty or not. And counts camel-case words	
		return (int) (str.isEmpty() ? 0 : 1 + str.chars().filter(Character::isUpperCase).count());
		//return 0;
		
	}
	
	int getNumberOfWords(String str){
        if(str==null || str.length()==0)
        	return 0;
        
        int words = 1;
        for(char c : str.toCharArray()){
            if(Character.isUpperCase(c)){
                words++;
            }
        }
        return words;
    }
	
}
