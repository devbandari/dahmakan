package com.my.dahmakan.task3;

import java.util.List;

public interface BackupService {
	 List<Delivery> deployBackupRider();    
	 void add(Delivery delivery); 
}
