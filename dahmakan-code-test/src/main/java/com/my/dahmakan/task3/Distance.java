package com.my.dahmakan.task3;

import java.time.Duration;
import java.time.Instant;

public class Distance implements DistanceService {

	@Override
	public int getDistance(Delivery a, Delivery b) {
		// TODO Auto-generated method stub
Duration duration = Duration.between(a.getToBeDeliveredAt(), b.getToBeDeliveredAt());
		return (int)duration.getSeconds();
	}

	@Override
	public int getDistanceFromKitchen(Delivery delivery) {	
		Duration duration = Duration.between(Instant.now(),delivery.getToBeDeliveredAt());
		return (int)duration.getSeconds();
	}

}
