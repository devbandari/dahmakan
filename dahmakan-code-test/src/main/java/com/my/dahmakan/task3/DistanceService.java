package com.my.dahmakan.task3;

public interface DistanceService {
	 int getDistance(Delivery a, Delivery b); 
	 int getDistanceFromKitchen(Delivery delivery); 
}
