package com.my.dahmakan.task3;

import java.time.Instant;

public interface Delivery {
	 String getId();   
	 Instant getToBeDeliveredAt();
}
