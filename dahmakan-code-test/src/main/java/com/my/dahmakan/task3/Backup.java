package com.my.dahmakan.task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Backup implements BackupService {

private List<Delivery> backupRides = new ArrayList<>();
List<Delivery> backupRidesTemp;

	@Override
	public List<Delivery> deployBackupRider() {
		// TODO Auto-generated method stub

		List<Delivery> filteredBackup = new ArrayList<>();
		if(backupRides.isEmpty() || backupRides.size() < 4)
		return null;

		backupRidesTemp = backupRides;
		
		Delivery nextDelivary = getShortestPointFromKitchen();
		filteredBackup.add(nextDelivary);
		for(int i=0; i<backupRidesTemp.size(); i++) {
			if(backupRidesTemp.size()>0) {
			nextDelivary = getShortestPoint(nextDelivary, backupRidesTemp);
			filteredBackup.add(nextDelivary);	
			}
		}		
		//Collections.sort(backupRides, (a,b) -> a.getToBeDeliveredAt().compareTo(b.getToBeDeliveredAt()));
		//filteredBackup.add(backupRides.get(0));
		
		
		return filteredBackup;
	}

	Delivery getShortestPointFromKitchen(){
		Distance distance = new Distance();
		int distanceMin=distance.getDistanceFromKitchen(backupRidesTemp.get(0));
		Delivery shortestDelivary = backupRidesTemp.get(0);
		int distanceTemp ;
		
	for(int i=1; i<backupRidesTemp.size(); i++) {
		 distanceTemp = distance.getDistanceFromKitchen(backupRidesTemp.get(i));
		 if(distanceMin>distanceTemp) {
			 distanceMin= distanceTemp;
			 shortestDelivary = backupRidesTemp.get(i);
			 backupRidesTemp.remove(i);			 
		 }
		}
	return shortestDelivary;
	}
	
	Delivery getShortestPoint(Delivery currentDelivary, List<Delivery> backupRidesTemp){
		Distance distance = new Distance();
		int distanceMin = distance.getDistance(currentDelivary, backupRidesTemp.get(0));
		Delivery shortestDelivary = backupRidesTemp.get(0);
		int distanceTemp ;
		
	for(int i=0; i<backupRidesTemp.size(); i++) {
		 distanceTemp = distance.getDistance(currentDelivary, backupRidesTemp.get(i));
		 if(distanceMin>distanceTemp) {
			 distanceMin= distanceTemp;
			 shortestDelivary = backupRidesTemp.get(i);
		 }
		}
	return shortestDelivary;
	}
	
	@Override
	public void add(Delivery delivery) {		
		backupRides.add(delivery);
	}

}
