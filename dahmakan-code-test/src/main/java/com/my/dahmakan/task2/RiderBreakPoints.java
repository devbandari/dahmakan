package com.my.dahmakan.task2;


public class RiderBreakPoints {

	Integer getBreakPoint(int[] rides) {
		int sum=0,sum1;
		for(int i=0; i<rides.length; i++) {
			sum+=rides[i];
			sum1=0;
			for(int j=i+1; j<rides.length;j++)
			{
				sum1+=rides[j];
				if(sum1>sum)
					break;
			}
			if(sum==sum1) 
				return i;		
			else if(sum>sum1)
				break;
		}
		
		// Note: In an integer list we will get only one point where condition may meet sum(0:i) equals to sum(i+1:n). So I am quite confused with bonus task.
		
		//return (int) (rides.length==0 ? null : IntStream.of(rides).collect(supplier, accumulator, combiner);
		return null;
		
	}
	
	
}
